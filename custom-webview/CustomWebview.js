import React, { useEffect, useState } from "react";
import { ActivityIndicator, LogBox, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { WebView } from "react-native-webview";
import styles from "./styles/CustomWebview.style";

LogBox.ignoreLogs(["Encountered an error loading page"]);

export function CustomWebview(props) {
  const [hasErrorWebView, setHasErrorWebview] = useState(false);
  const { uri, disableCache, ref, onNavigationStateChange } = props;

  useEffect(() => {
    if (!hasErrorWebView) {
      setHasErrorWebview(false);
    }
  }, [hasErrorWebView]);

  const onWebviewError = () => {
    setHasErrorWebview(true);
  };

  const reloadWebview = () => setHasErrorWebview(false);

  return (
    <View style={styles.container}>
      {!hasErrorWebView && (
        <WebView
          ref={ref}
          startInLoadingState
          source={{ uri }}
          renderLoading={() => (
            <ActivityIndicator
              color="black"
              size="large"
              style={styles.activityIndicator}
            />
          )}
          onNavigationStateChange={onNavigationStateChange}
          cacheEnabled={disableCache ? false : true}
          javaScriptEnabled
          onError={onWebviewError}
          style={styles.webview}
        />
      )}
      {hasErrorWebView && (
        <TouchableOpacity style={styles.webviewError} onPress={reloadWebview}>
          <Text style={styles.textWebviewError}>Error Loading Page</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}
