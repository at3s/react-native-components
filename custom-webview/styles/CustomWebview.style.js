import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1, position: 'relative'},
  activityIndicator: {
    position: 'absolute',
    alignSelf: 'center',
    top: '50%',
  },
  webview: {
    minHeight: '100%',
    height: '100%',
    opacity: 0.99,
    flex: 1,
  },
  webviewError: {
    position: 'relative',
    width: '100%',
    height: '100%',
    paddingHorizontal: 20,
  },
  textWebviewError: {
    alignSelf: 'center',
    position: 'absolute',
    top: '50%',
    textAlign: 'center',
    fontSize: 20,
  },
});

export default styles;
