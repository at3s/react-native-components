import React from "react";
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StatusBar,
  SafeAreaView,
} from "react-native";
import { fixed, scroll } from "./screen.preset";
const isIos = Platform.OS === "ios";

const ScreenWithScroll = (props) => {
  const {
    style,
    statusBarTranslucent,
    statusBarBackgroundColor,
    hideStatusbar,
    statusbarStyle,
    children,
  } = props;

  return (
    <KeyboardAvoidingView
      behavior={isIos ? "padding" : null}
      keyboardVerticalOffset={"none"}
      style={[scroll.outer, style]}
    >
      <StatusBar
        translucent={statusBarTranslucent}
        backgroundColor={
          statusBarBackgroundColor ? statusBarBackgroundColor : "#00213b"
        }
        hidden={hideStatusbar || false}
        barStyle={statusbarStyle || "light-content"}
      />
      <SafeAreaView style={scroll.inner}>
        <ScrollView keyboardShouldPersistTaps="always">{children}</ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const ScreenWithoutScroll = (props) => {
  const {
    style,
    statusBarTranslucent,
    statusBarBackgroundColor,
    hideStatusbar,
    statusbarStyle,
    children,
  } = props;

  return (
    <KeyboardAvoidingView
      behavior={isIos ? "padding" : null}
      keyboardVerticalOffset={"none"}
      style={[fixed.outer, style]}
    >
      <StatusBar
        translucent={statusBarTranslucent}
        backgroundColor={
          statusBarBackgroundColor ? statusBarBackgroundColor : "#00213b"
        }
        hidden={hideStatusbar || false}
        barStyle={statusbarStyle || "light-content"}
      />
      <SafeAreaView style={fixed.inner}>{children}</SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export function Screen(props) {
  if (props.scroll) {
    return <ScreenWithScroll {...props} />;
  } else {
    return <ScreenWithoutScroll {...props} />;
  }
}

export default Screen;
