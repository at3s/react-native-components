import { StyleSheet } from "react-native";
import { vertical } from "../theme/responsive";

const styles = StyleSheet.create({
  wrapper: {
    width: "100%",
    height: vertical(170),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: vertical(35),
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB",
    width: "100%",
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5",
    width: "100%",
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9",
    width: "100%",
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold",
  },
  imgSlide: {
    width: "100%",
    height: "100%",
  },
  paginationStyle: {
    position: "absolute",
    bottom: -15,
  },
  dotStyle: {
    backgroundColor: "#fff",
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
    borderColor: "#00213b",
    borderWidth: 1,
    borderStyle: "solid",
  },
});

export default styles;
