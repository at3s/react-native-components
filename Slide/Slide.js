import React, { useState } from "react";
import { ActivityIndicator, View } from "react-native";
import FastImage from "react-native-fast-image";
import Swiper from "react-native-swiper";
import styles from "./Slide.style";

export function Slide(props) {
  return (
    <View style={styles.wrapper}>
      <Swiper
        loadMinimalLoader={<ActivityIndicator size={30} color={"#00213b"} />}
        paginationStyle={styles.paginationStyle}
        activeDotColor={"#00213b"}
        dot={<View style={styles.dotStyle} />}
      >
        <View style={styles.slide1}>
          <FastImg uri={props.slides[0]} />
        </View>
        <View style={styles.slide2}>
          <FastImg uri={props.slides[1]} />
        </View>
        <View style={styles.slide3}>
          <FastImg uri={props.slides[2]} />
        </View>
      </Swiper>
    </View>
  );
}

const FastImg = (props) => {
  const { uri } = props;
  const [imgLoading, setImgLoading] = useState(true);
  const onImgLoadEnd = () => setImgLoading(false);

  return (
    <FastImage
      source={{ uri }}
      resizeMode={FastImage.resizeMode.cover}
      style={styles.imgSlide}
      onLoadEnd={onImgLoadEnd}
      fallback
    >
      {imgLoading && <ActivityIndicator size={35} color={"#00213b"} />}
    </FastImage>
  );
};
